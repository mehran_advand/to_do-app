//var
let form=document.querySelector('#task-form');
let taskList=document.querySelector('.collection');
let filter=document.querySelector('#filter');
let taskInput=document.querySelector('#task');
let clearTask=document.querySelector('.clear-task');
//invoke function 
loadAllEvent();

function loadAllEvent(){
    document.addEventListener('DOMContentLoaded',getTasks);
    //addTask
    form.addEventListener('submit', addTask);
    //removeTask
    taskList.addEventListener('click',removeTask);
    //removeAll
    clearTask.addEventListener('click',removeAll);
    //filter
    filter.addEventListener('keyup',filterword);
}

//addTask
function addTask(e){
   e.preventDefault(); 
   if(taskInput.value===''){
       alert('please enter your task');
   }
   let li=document.createElement('li');
   li.className="collection-item";
   li.appendChild(document.createTextNode(taskInput.value));
   let a=document.createElement('a');
   a.className="delete-item secondary-content";
   a.innerHTML='<i class="fa fa-remove"></i>';
   li.appendChild(a);
   taskList.appendChild(li);
   storageTaskInLS(taskInput.value);
   taskInput.value="";
}
//removeTask

function removeTask(e){
    if(e.target.parentElement.classList.contains('delete-item')){
        if(confirm('Are You sure?')){
            e.target.parentElement.parentElement.remove();
            removeLS(e.target.parentElement.parentElement);
        }
    }
}

//removeAll
function removeAll(e){
    e.preventDefault();
    while(taskList.firstChild){
        taskList.removeChild(taskList.firstChild);
    }
    removeLS();
}
//filter
function filterword(){
    let text=filter.value.toLowerCase();
    document.querySelectorAll('.collection-item').forEach((li) => {
        let content=li.textContent.toLocaleLowerCase();
        if(content.indexOf(text)!=-1){
            li.style.display='block';
        }
        else{
            li.style.display='none';
        }
    });
}

//storage in LS
function storageTaskInLS(taskItem){
    let tasks;
    if(localStorage.getItem('tasks')===null){
        tasks=[];
    }
    else{
        tasks=JSON.parse(localStorage.getItem('tasks'));
    }
    tasks.push(taskItem);
    localStorage.setItem('tasks',JSON.stringify(tasks));
}
//get form LS
function getTasks(){
    let tasks;
    if(localStorage.getItem('tasks')===null){
        tasks=[];
    }
    else{
        tasks=JSON.parse(localStorage.getItem('tasks'));
    }
    tasks.forEach((task) => {
        let li=document.createElement('li');
        li.className="collection-item";
        li.appendChild(document.createTextNode(task));
        let a=document.createElement('a');
        a.className="delete-item secondary-content";
        a.innerHTML='<i class="fa fa-remove"></i>';
        li.appendChild(a);
        taskList.appendChild(li); 
    })
};
//remove from LS
function removeLS(taskItem){
    let tasks;
    if(localStorage.getItem('tasks')===null){
        tasks=[];
    }
    else{
        tasks=JSON.parse(localStorage.getItem('tasks'));
    } 
    tasks.forEach((task,index) => {
        if(taskItem.textContent===task){
            tasks.splice(index,1);
        }
    });
    localStorage.setItem('tasks',JSON.stringify(tasks));
}
//remove all
function removeLS(){
    localStorage.clear();
}
